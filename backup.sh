#! /bin/sh

set -e
set -o pipefail

# set the filename from FILENAME env or default to backup_$(date +"%Y-%m-%dT%H:%M:%SZ").tar.gz
FILENAME=${FILENAME:-backup_$(date +"%Y-%m-%dT%H:%M:%SZ").tar.gz}

if [ "${S3_ACCESS_KEY_ID}" = "**None**" ]; then
  echo "You need to set the S3_ACCESS_KEY_ID environment variable."
  exit 1
fi

if [ "${S3_SECRET_ACCESS_KEY}" = "**None**" ]; then
  echo "You need to set the S3_SECRET_ACCESS_KEY environment variable."
  exit 1
fi

if [ "${S3_BUCKET}" = "**None**" ]; then
  echo "You need to set the S3_BUCKET environment variable."
  exit 1
fi


if [ "${SOURCE_PATH}" = "**None**" ]; then
  echo "You need to set the SOURCE_PATH environment variable."
  exit 1
fi

if [ "${S3_ENDPOINT}" == "**None**" ]; then
  AWS_ARGS=""
else
  AWS_ARGS="--endpoint-url ${S3_ENDPOINT}"
fi

# env vars needed for aws tools
export AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
export AWS_DEFAULT_REGION=$S3_REGION

echo "Creating backup of files from ${SOURCE_PATH}..."

tar -czf /tmp/archive.tar.gz ${SOURCE_PATH}

echo "Uploading dump file as ${FILENAME} to ${S3_BUCKET}"

aws ${AWS_ARGS} s3 cp /tmp/archive.tar.gz s3://${S3_BUCKET}/${S3_PREFIX}/${FILENAME} || exit 2

rm /tmp/archive.tar.gz

echo "Files backup uploaded successfully"
