# directory-backup-s3

Backup selected directory to S3 (supports periodic backups).


## Usage

Docker:
```sh
$ docker run -e S3_ACCESS_KEY_ID=key -e S3_SECRET_ACCESS_KEY=secret -e S3_BUCKET=my-bucket -e S3_PREFIX=backup -e SOURCE_PATH=/data/db registry.gitlab.com/engrave/directory-backup-s3
```

Docker Compose:
```yaml
dirbackup:
  image: registry.gitlab.com/engrave/directory-backup-s3:latest
  environment:
    SCHEDULE: '@daily'
    S3_REGION: region
    S3_ACCESS_KEY_ID: key
    S3_SECRET_ACCESS_KEY: secret
    S3_BUCKET: my-bucket
    S3_PREFIX: backup
    SOURCE_PATH: /data/db
    FILENAME: files.tar.gz # optional
  volumes:
    db_data:/data/db
```

Optional environment variables:

```
FILENAME: Filename of the backup (default: backup_$(date +"%Y-%m-%dT%H:%M:%SZ").tar.gz, example backup_2020-01-01T00:00:00Z.sql.gz)
```

### Automatic Periodic Backups

You can additionally set the `SCHEDULE` environment variable like `-e SCHEDULE="@daily"` to run the backup automatically.

More information about the scheduling can be found [here](http://godoc.org/github.com/robfig/cron#hdr-Predefined_schedules).

### S3 versioning with static filename
If you configure your s3 bucket to version your object, you can use static filename which will override the previous backup. This is useful if you want to keep only the latest backup. You can also configure some lifecycle rules to delete old backups.
